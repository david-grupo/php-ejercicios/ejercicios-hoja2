<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="ejercicio11.css"> 
    </head>
    <body>
        <div class="container">
        <h1>Formulario de inscripción de usuarios</h1>
        <form name="inscripcion" id="ficha">
            <div class="wrapper">
                <div class="columna1">
                    <div id="a"><label for="nombre">Nombre completo</label></div>
                    <div id="b"><label for="direccion">Dirección</label></div>
                    <div id="a"><label for="email">Correo Electrónico</label></div>
                    <div id="a"><label for="contraseña">Contraseña</label></div>
                    <div id="ab"><label for="confirmar">Confirmar contraseña</label></div>
                    <div id="a"><label for="nacimiento">Fecha de nacimiento</label></div>
                    <div id="ab"><label for="sexo">Sexo</label></div>
                    <div id="a"><label for="aficiones">Por favor elija los temas de su interés</label></div>
                    <div id="ab"><label for="otras">Selecciona tus aficiones</label></div>
                    <div id="a"><label for="otrasb">(Selecciona múltiples
                        elementos pulsando la tecla Control y haciendo clic en cada uno, uno a uno)
                            </label></div>
                   
                </div>
                <div class="columna2">
                    <div id="a"><input type="text" name="nombre"></div>
                    <div id="b"><input type="text" name="direccion"></div>
                    <div id="a"><input type="text" name="email"></div>
                    <div id="a"><input type="text" name="contrasena"></div>
                    <div id="ab"><input type="text" name="confirmar"></div>
                    <div id="a"><input type="date" name="nacimiento"></div>
                    <div id="ab">
                        <input type="radio" name="sexo" value="hombre">Hombre
                        <input type="radio" name="sexo" value="mujer">Mujer
                    </div>
                    <div id="a">
                        <input type="checkbox" name="aficiones" value="ficcion">Ficción
                        <input type="checkbox" name="aficiones" value="terror">Terror
                        <input type="checkbox" name="aficiones" value="accion">Acción
                        <input type="checkbox" name="aficiones" value="comedia">Comedia
                        <input type="checkbox" name="aficiones" value="suspense">Suspense
                    </div>
                    <div id="abc">
                        <select multiple name="otras[]">
                            <option value="1">Deportes al aire libre</option>
                            <option value="2">Deportes de aventuras</option>
                            <option value="3">Música pop</option>
                            <option value="4">Música rock</option>
                            <option value="5">Música alternativa</option>
                            <option value="6">Fotografía</option>
                        </select>
                    </div>
                </div>    
                <div><button>Enviar</button></div>
            </div>
        </form>
        </div>
    </body>
</html>

<?php
/*if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}*/
?>
<!--<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="ejercicio11.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <h1>Formulario de inscripción de usuarios</h1>-->
        <?php
      /*  if ($mal) {
            ?>
            <form class="formulario" name = "f">
                <div class="fila">
                    <label for="nombre">Nombre Completo</label><div class="valor"><input name="nombre" id="nombre" type="text"></div>
                </div>
                <div class="fila">
                    <label for="direccion">Dirección</label><div class="valor"><textarea name="direccion" id="direccion"></textarea></div>
                </div>
                <div class="fila">
                    <label for="email">Correo<br>Electronico</label><div class="valor"><input name="email" id="email" type="text"></div>
                </div>
                <div class="fila">
                    <label for="password">Contraseña</label><div class="valor"><input name="password" id="password" type="password"></div>
                </div>
                <div class="fila">
                    <label for="cpassword">Confirmar<br>Contraseña</label><div class="valor"><input name="cpassword" id="cpassword" type="password"></div>
                </div>
                <div class="fila">
                    <label for="nacimiento">
                        Fecha de<br>Nacimiento
                    </label>
                    <div class="valor">
                        <select name="mes" id="nacimiento">
                            <option selected value=1>Enero </option>
                            <option value=2>Febrero </option>
                            <option value=3>Marzo </option>
                            <option value=4>Abril </option>
                            <option value=5>Mayo </option>
                            <option value=6>Junio </option>
                            <option value=7>Julio </option>
                            <option value=8>Agosto </option>
                            <option value=9>Septiembre </option>
                            <option value=10>Octubre</option> 
                            <option value=11>Noviembre </option>
                            <option value=12>Diciembre</option>
                        </select> 
                        <select name="dia">
                            <option selected value=1>01 </option>
                            <option value=2>02 </option>
                            <option value=3>03 </option>
                            <option value=4>04 </option>
                            <option value=5>05 </option>
                            <option value=6>06 </option>
                            <option value=7>07 </option>
                            <option value=8>08 </option>
                            <option value=9>09 </option>
                            <option value=10>10 </option>
                            <option value=11>11 </option>
                            <option value=12>12 </option>
                            <option value=13>13 </option>
                            <option value=14>14 </option>
                            <option value=15>15 </option>
                            <option value=16>16 </option>
                            <option value=17>17 </option>
                            <option value=18>18 </option>
                            <option value=19>19 </option>
                            <option value=20>20 </option>
                            <option value=21>21 </option>
                            <option value=22>22 </option>
                            <option value=23>23 </option>
                            <option value=24>24 </option>
                            <option value=25>25 </option>
                            <option value=26>26 </option>
                            <option value=27>27 </option>
                            <option value=28>28 </option>
                            <option value=29>29 </option>
                            <option value=30>30 </option>
                            <option value=31>31</option>
                        </select>
                        <input name="year" type="text" placeholder="YYYY">
                    </div>
                </div>
                <div class="fila">
                    <label for="sexo">
                        Sexo
                    </label>
                    <div class="valor">
                        <input type="radio" name="sexo" id="hombre" value="hombre" />
                        <label for="hombre">Hombre</label>
                        <input type="radio" name="sexo" id="mujer" value="mujer" />
                        <label for="mujer">Mujer</label>
                    </div>
                </div>
                <div class="fila">
                    <label for="intereses">
                        Por favor elige<br>los temas de tus<br>intereses
                    </label>
                    <div class="valor">
                        <input type="checkbox" name="intereses[]" id="ficcion" value="Ficcion" />
                        <label for="ficcion">Ficcion</label>
                        <input type="checkbox" name="intereses[]" id="terror" value="terror" />
                        <label for="terror">Terror</label>
                        <input type="checkbox" name="intereses[]" id="accion" value="accion" />
                        <label for="accion">Acción</label>
                        <input type="checkbox" name="intereses[]" id="comedia" value="comedia" />
                        <label for="comedia">Comedia</label>
                        <input type="checkbox" name="intereses[]" id="suspense" value="suspense" />
                        <label for="suspense">Suspense</label>
                    </div>
                </div>
                <div class="fila">
                    <label for="aficiones">
                        Selecciona tus<br>aficiones<br><br>
                        (Selecciona multiples elementos pulsando la tecla Control y haciendo clic en cada uno, uno a uno)
                    </label>
                    <div class="valor">
                        <select id="aficiones" name="aficiones[]" size="7" multiple>
                            <option value="Deportes al aire libre">Deportes al aire libre</option>
                            <option value="Deportes de aventuras">Deportes de aventuras</option>
                            <option value="Musica Pop">Musica Pop</option>
                            <option value="Musica Rock">Musica Rock</option>
                            <option value="Musica alternativa">Musica alternativa</option>
                            <option value="Fotografia">Fotografia</option>
                        </select>
                    </div>
                </div>
                <div class="pie">
                    <input type="submit" value="Enviar" name="boton" />
                </div>
            </form>



            <?php
        } else {

            echo "los elementos seleccionados son: ";
            foreach ($_REQUEST as $key => $value) {
                if (gettype($value) == 'array') {
                    foreach ($value as $k => $v) {
                        echo "<br>$k-$v";
                    }
                } else {
                    echo "<br>$key-$value";
                }
            }
        }
        ?>        

    </body>
</html>


